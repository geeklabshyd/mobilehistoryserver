package com.geeklabs.mobilehistory.util;

public class RequestMapper {
	
	public final static String USER = "user";

	public final static String USER_LOGIN_SUCCESS = "/login/success";
	public final static String USER_LOGIN_ERROR = "/login/error";
	public static final String IS_ACTIVATED = "/activate";
	
	/**Admin**/
	public static final String ADMIN = "/admin";
	public static final String SETUP = "/admin/setup";
	
	/**AUTHENTICATION**/
	public static final String AUTHENTICATION = "/authentication";
	public static final String SIGNIN = "/signIn";
	public static final String SIGNOUT = "/signout/{userId}";
	
	/** User **/
	public static final String MY_ACCOUNT = "/myAccount";
	public static final String MY_ACCOUNT_GET_USER = "/myAccount/getUserInfo";
	
	/**Call History**/
	public static final String CALL = "/call";
	public static final String SAVE_CALL_HISTORY = "/saveCallHistory/{userId}";
	public static final String CLEAR_CALL_INFO = "/clearCallInfo/{userId}";
	public static final String CALL_LIST = "/list";
	public static final String GET_CALL_LIST = "/list/get/{pageNo}";
	public static final String DELETE_CALL = "/delete/{callId}";
	//public static final String GET_CALLS_BY_STATUS_WITH_PAGINATION = "/list/get/{pageNo}";
	public static final String GET_ALL_CALLS_FOR_MOBILE = "/getAllCalls/{userId}";
	public static final String GET_CALLS_WITH_PAGINATION_SEARCH = "/list/get/{pageNo}/{searchType}/{searchTerm}";
	
	/**SMS**/
	public static final String SMS = "/sms";
	public static final String SAVE_SMS = "/saveSms/{userId}";
	public static final String GET_SMS_INFO = "/getSmsInfo/{userId}";
	public static final String CLEAR_SMS_INFO = "/clearSmsInfo/{userId}";
	public static final String GET_SMS_LIST_BY_PAGABLE = "/list/get/{pageNo}";
	public static final String GET_SMS_LIST_BY_PAGABLE_AND_SEARCH = "/list/get/{pageNo}/{searchType}/{searchTerm}";
	public static final String SMS_LIST = "/list";
	public static final String DELETE_SMS = "/delete/{smsId}";
	public static final String GET_SMS_LIST = "/list/get";
	
	
	/**personalize**/
	public static final String PERSONLIZE = "/personalize";
	public static final String SAVE_PERSONLIZE = "/save/{userId}";
	
	/**android request**/
	public static final String GET_SMS_HISTORY_FOR_MOBILE = "/getSmsHistory/{userId}";
	public static final String GET_CALLS_FOR_MOBILE = "/getAllCalls/{userId}";
	
	
	/** phone book **/
	public static final String PHONE_BOOK = "/phonebook";
	public static final String SAVE_PHONE_BOOK = "/savePhoneBook/{userId}";
	public static final String CONTACT_LIST = "/list";
	public static final String GET_CONTACT_LIST = "/list/get/{pageNo}";
	public static final String GET_CONTACTS_FOR_MOBILE = "/getAllContacts";
	public static final String GET_CONTACT_LIST_PAGABLE_SEARCH = "/list/get/{pageNo}/{searchType}/{searchTerm}";

	/** sync **/
	public static final String SYNC = "/sync";
	public static final String SYNC_NOW = "/syncNow/{userId}";
	/**Forgot Password**/
	public static final String USER_FORGOTPASSWORD = "/forgotPassword";
	
	
	/**delete records**/
	public static final String DELETED = "/delete";
	public static final String GET_DELETED_RECORDS = "/getDeletedRecords/{userId}";




	
}
