package com.geeklabs.mobilehistory.util;

public class Constants {

		private Constants(){}
		// UI Constants
		public static final String ALL_CALLS = "ALL";
		public static final String MISSED_UI = "MISSED";
		public static final String RECIEVED = "RECIEVED";
		public static final String DIALED = "DIALED";
		
		//for Mobile call types
		public static final int INCOMING = 1;
		public static final int OUTGOING = 2;
		public static final int MISSED = 3;
		
		
		public static final int INBOX = 1;
		public static final int SENT = 2;
		
		public static final String INBOX_UI = "RECEIVED";
		public static final String SENT_UI = "SENT";
}
