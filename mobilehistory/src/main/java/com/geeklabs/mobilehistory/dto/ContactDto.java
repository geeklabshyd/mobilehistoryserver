package com.geeklabs.mobilehistory.dto;


public class ContactDto {

	private Long serverContactId;
	private String name;
	private String number;
	private Long userId;
	private boolean isSyncked;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Long getServerContactId() {
		return serverContactId;
	}
	public void setServerContactId(Long serverContactId) {
		this.serverContactId = serverContactId;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public boolean isSyncked() {
		return isSyncked;
	}
	public void setSyncked(boolean isSyncked) {
		this.isSyncked = isSyncked;
	}
	
}
