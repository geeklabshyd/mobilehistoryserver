package com.geeklabs.mobilehistory.dto;


public class DeletedRecordsDto {

	private Long id;
	private Long serverId;
	private Long userId;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getServerId() {
		return serverId;
	}
	
	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}
}
