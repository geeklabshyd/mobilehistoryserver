package com.geeklabs.mobilehistory.dto;

import java.util.ArrayList;
import java.util.List;

import com.geeklabs.mobilehistory.domain.DeletedRecords;

public class SyncDto {
	private List<SMSDto> smses = new ArrayList<>();
	private List<CallLogsDto>  callLogs = new ArrayList<>();  
	private List<DeletedRecords> deletedRecords = new  ArrayList<>();
	
	private boolean isAllSynced;
	private boolean isSmsSynced;
	private boolean isCallsSynced;
	private boolean isContactsSynced;
	
	public boolean isAllSynced() {
		return isAllSynced;
	}
	public void setAllSynced(boolean isAllSynced) {
		this.isAllSynced = isAllSynced;
	}
	public boolean isSmsSynced() {
		return isSmsSynced;
	}
	public void setSmsSynced(boolean isSmsSynced) {
		this.isSmsSynced = isSmsSynced;
	}
	public boolean isCallsSynced() {
		return isCallsSynced;
	}
	public void setCallsSynced(boolean isCallsSynced) {
		this.isCallsSynced = isCallsSynced;
	}
	public boolean isContactsSynced() {
		return isContactsSynced;
	}
	public void setContactsSynced(boolean isContactsSynced) {
		this.isContactsSynced = isContactsSynced;
	}
	public List<SMSDto> getSmses() {
		return smses;
	}
	public void setSmses(List<SMSDto> smses) {
		this.smses = smses;
	}
	public List<DeletedRecords> getDeletedRecords() {
		return deletedRecords;
	}
	public void setDeletedRecords(List<DeletedRecords> deletedRecords) {
		this.deletedRecords = deletedRecords;
	}
	public List<CallLogsDto> getCallLogs() {
		return callLogs;
	}
	public void setCallLogs(List<CallLogsDto> callLogs) {
		this.callLogs = callLogs;
	}
}
