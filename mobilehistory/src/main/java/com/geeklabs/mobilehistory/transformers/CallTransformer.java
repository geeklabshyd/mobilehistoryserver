package com.geeklabs.mobilehistory.transformers;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.mobilehistory.domain.CallLogs;
import com.geeklabs.mobilehistory.dto.CallLogsDto;
import com.geeklabs.mobilehistory.util.Constants;

public class CallTransformer {

	public static List<CallLogsDto> convertCallListIntoCallLogsDtoList(DozerBeanMapper dozerBeanMapper, List<CallLogs> callHistories) {

		List<CallLogsDto> CallLogsDtos = new ArrayList<>();
		
		for (CallLogs CallLogs : callHistories) {
			CallLogsDto CallLogsDto = dozerBeanMapper.map(CallLogs, CallLogsDto.class);
			
			switch (CallLogsDto.getCallType()) {
				case Constants.INCOMING:
					CallLogsDto.setActualCallType(Constants.RECIEVED);
					break;
				case Constants.OUTGOING:
					CallLogsDto.setActualCallType(Constants.DIALED);	
					break;
				case Constants.MISSED:
					CallLogsDto.setActualCallType(Constants.MISSED_UI);
					break;
	
				default:
					break;
			}
			
			CallLogsDtos.add(CallLogsDto);
		}
		
		return CallLogsDtos;
	}

}
