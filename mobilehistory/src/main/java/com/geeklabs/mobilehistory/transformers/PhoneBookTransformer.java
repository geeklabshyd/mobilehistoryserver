package com.geeklabs.mobilehistory.transformers;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.mobilehistory.domain.Contact;
import com.geeklabs.mobilehistory.dto.ContactDto;

public class PhoneBookTransformer {

	public static List<ContactDto> convertContactsListIntoContactsDtoList(DozerBeanMapper dozerBeanMapper, List<Contact> contacts) {
		List<ContactDto> contactDtos = new ArrayList<>();

		for (Contact contact : contacts) {
			ContactDto contactDto = dozerBeanMapper.map(contact, ContactDto.class);
			contactDtos.add(contactDto);
		}

		return contactDtos;
	}

}
