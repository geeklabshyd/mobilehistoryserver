package com.geeklabs.mobilehistory.transformers;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.mobilehistory.domain.SMS;
import com.geeklabs.mobilehistory.dto.SMSDto;
import com.geeklabs.mobilehistory.util.Constants;

public class SMSTransformer {

	public static List<SMSDto> convertSMSListIntoSMSDtosList(List<SMS> smsList, DozerBeanMapper dozerBeanMapper) {
		
		List<SMSDto> smsDtos = new ArrayList<SMSDto>();
		
		for (SMS sms : smsList) {
			SMSDto smsDto = dozerBeanMapper.map(sms, SMSDto.class);
			switch (smsDto.getType()) {
			case Constants.INBOX:
				smsDto.setActualSmsType(Constants.INBOX_UI);
				break;

			case Constants.SENT:
				smsDto.setActualSmsType(Constants.SENT_UI);
				break;
			}
			smsDtos.add(smsDto);
		}
		
		return smsDtos;
	}
}
