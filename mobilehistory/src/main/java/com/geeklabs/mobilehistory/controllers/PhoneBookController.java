package com.geeklabs.mobilehistory.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.domain.CustomUserDetails;
import com.geeklabs.mobilehistory.dto.ContactDto;
import com.geeklabs.mobilehistory.service.PhoneBookService;
import com.geeklabs.mobilehistory.util.RequestMapper;

@Controller
@RequestMapping(value = RequestMapper.PHONE_BOOK)
public class PhoneBookController {

	@Autowired
	private PhoneBookService phoneBookService;

	@RequestMapping(value = RequestMapper.CONTACT_LIST, method = RequestMethod.GET)
	public String getContactsList() {
		return "contacts";
	}

	@RequestMapping(value = RequestMapper.SAVE_PHONE_BOOK, method = RequestMethod.POST)
	public @ResponseBody List<ContactDto> savePhoneBook(@PathVariable String userId, @RequestBody List<ContactDto> contactDtos) {
		return phoneBookService.savePhoneBook(userId, contactDtos);
	}

	@RequestMapping(value = RequestMapper.GET_CONTACT_LIST, method = RequestMethod.GET)
	public @ResponseBody List<ContactDto> getContactList(Principal principal, @PathVariable int pageNo) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return phoneBookService.getAllContactsByPagableAndSearchType(userDetails.getId(), pageNo, null,  "");
	}
	
	@RequestMapping(value = RequestMapper.GET_CONTACT_LIST_PAGABLE_SEARCH, method = RequestMethod.GET)
	public @ResponseBody List<ContactDto> getContactList(Principal principal, @PathVariable int pageNo, @PathVariable String searchType, @PathVariable String searchTerm) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return phoneBookService.getAllContactsByPagableAndSearchType(userDetails.getId(), pageNo, searchType, searchTerm);
	}
	
	//for mobile
	@RequestMapping(value = RequestMapper.GET_CONTACTS_FOR_MOBILE, method = RequestMethod.GET)
	public @ResponseBody List<ContactDto> getContactsByPagable(@PathVariable String userId, @RequestParam String currentPage) {
		return phoneBookService.getContactsByPagable(userId, currentPage);
	}
}

