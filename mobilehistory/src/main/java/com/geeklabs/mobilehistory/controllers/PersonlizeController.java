package com.geeklabs.mobilehistory.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.dto.PersonalizeDto;
import com.geeklabs.mobilehistory.service.PersonlizeService;
import com.geeklabs.mobilehistory.util.RequestMapper;
import com.geeklabs.mobilehistory.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.PERSONLIZE)
public class PersonlizeController {

	@Autowired
	private PersonlizeService personlizeService;

	@RequestMapping(value = RequestMapper.SAVE_PERSONLIZE, method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus savePersonlize(@PathVariable String userId, @RequestBody PersonalizeDto personlizeDto) {
		return personlizeService.savepersonlize(userId, personlizeDto);
	}

}