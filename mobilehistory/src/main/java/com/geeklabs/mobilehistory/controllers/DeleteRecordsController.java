package com.geeklabs.mobilehistory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.dto.DeletedRecordsDto;
import com.geeklabs.mobilehistory.service.DeletedRecordsService;
import com.geeklabs.mobilehistory.util.RequestMapper;

@Controller
@RequestMapping(value = RequestMapper.DELETED)
public class DeleteRecordsController {
	
	@Autowired
	private DeletedRecordsService deletedRecordsService;

	@RequestMapping(value = RequestMapper.GET_DELETED_RECORDS, method = RequestMethod.GET)
	public @ResponseBody List<DeletedRecordsDto> getAllDeletedRecords(@RequestParam int currentPage, @PathVariable String userId) {
		return deletedRecordsService.getDeletedRecordsByPagable(userId, currentPage);
	}
	
}
