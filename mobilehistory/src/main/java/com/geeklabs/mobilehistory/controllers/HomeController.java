package com.geeklabs.mobilehistory.controllers;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller 
public class HomeController {

	@RequestMapping(value = "/" , method = RequestMethod.GET)
	public String login(Model map, Principal principal) {
		
		if (principal != null) {
			return "redirect:/call/list";
		}
		return "login";
	}

	@RequestMapping(value = "/home" , method = RequestMethod.GET)
	public String home(Model map) {
		return "home";
	}
}
