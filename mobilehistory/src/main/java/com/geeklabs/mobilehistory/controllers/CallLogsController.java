package com.geeklabs.mobilehistory.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.domain.CustomUserDetails;
import com.geeklabs.mobilehistory.dto.CallLogsDto;
import com.geeklabs.mobilehistory.service.CallLogsService;
import com.geeklabs.mobilehistory.util.RequestMapper;
import com.geeklabs.mobilehistory.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.CALL)
public class CallLogsController {

	@Autowired
	private CallLogsService callLogsService;
	
	// Front Req
	@RequestMapping(value = RequestMapper.CALL_LIST, method = RequestMethod.GET)
	public String callHistory() {
		return "calls";
	}

	/*@RequestMapping(value = RequestMapper.GET_CALL_LIST, method = RequestMethod.GET)
	public @ResponseBody List<CallLogsDto> getCallHistory(Principal principal, @PathVariable int pageNo) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return callLogsService.getCallLogs(userDetails.getId(), pageNo);
	}*/
	
	@RequestMapping(value = RequestMapper.DELETE_CALL, method = RequestMethod.GET)
	public @ResponseBody ResponseStatus deleteCall(Principal principal, @PathVariable Long callId) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return callLogsService.deleteCall(callId, userDetails.getId());
	}
	
	@RequestMapping(value = RequestMapper.GET_CALL_LIST, method = RequestMethod.GET)
	public @ResponseBody List<CallLogsDto> getCallsByStatus(Principal principal, @PathVariable int pageNo) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return callLogsService.getCallsByStatus(userDetails.getId(), null, pageNo, "");
	}
	
	
	@RequestMapping(value = RequestMapper.GET_CALLS_WITH_PAGINATION_SEARCH, method = RequestMethod.GET)
	public @ResponseBody List<CallLogsDto> getCallsByStatus(Principal principal, @PathVariable int pageNo, @PathVariable String searchType, @PathVariable String searchTerm) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return callLogsService.getCallsByStatus(userDetails.getId(), searchType, pageNo, searchTerm);
	}
	
	
	//From Mobile Req
	
	@RequestMapping(value = RequestMapper.SAVE_CALL_HISTORY, method = RequestMethod.POST)
	public @ResponseBody List<CallLogsDto> saveCallHistory(@PathVariable String userId, @RequestBody List<CallLogsDto> CallLogsDtos) {
		return callLogsService.saveCallLogs(userId, CallLogsDtos);
	}
	
	@RequestMapping(value = RequestMapper.GET_CALLS_FOR_MOBILE, method = RequestMethod.GET)
	public @ResponseBody List<CallLogsDto> getCallLogsByPagableAndUserId(@PathVariable String userId, @RequestParam int currentPage) {
		return callLogsService.getCallLogsByPagable(userId, currentPage);
	}
}