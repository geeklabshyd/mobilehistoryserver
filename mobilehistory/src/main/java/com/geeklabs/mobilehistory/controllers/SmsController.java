package com.geeklabs.mobilehistory.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.domain.CustomUserDetails;
import com.geeklabs.mobilehistory.dto.SMSDto;
import com.geeklabs.mobilehistory.service.SmsService;
import com.geeklabs.mobilehistory.util.RequestMapper;
import com.geeklabs.mobilehistory.util.ResponseStatus;
@Controller
@RequestMapping(value= RequestMapper.SMS)
public class SmsController {

	@Autowired
	private SmsService smsService;


	//front end
	@RequestMapping(value = RequestMapper.SMS_LIST, method = RequestMethod.GET)
	public String getSmsList() {
		return "sms";
	}
	
	@RequestMapping(value = RequestMapper.GET_SMS_LIST_BY_PAGABLE, method = RequestMethod.GET)
	public @ResponseBody List<SMSDto> getSMSByPagination(Principal principal, @PathVariable int pageNo) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return smsService.getAllSMS(userDetails.getId(), pageNo, null, "");
	}
	
	@RequestMapping(value = RequestMapper.GET_SMS_LIST_BY_PAGABLE_AND_SEARCH, method = RequestMethod.GET)
	public @ResponseBody List<SMSDto> getSMSByPageAndSearch(Principal principal, @PathVariable int pageNo, @PathVariable String searchType, @PathVariable String searchTerm) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return smsService.getAllSMS(userDetails.getId(), pageNo,  searchType, searchTerm);
	}
	
	@RequestMapping(value = RequestMapper.DELETE_SMS, method = RequestMethod.GET)
	public @ResponseBody ResponseStatus deleteSms(@PathVariable Long smsId, Principal principal) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return smsService.deleteSms(smsId, userDetails.getId());
	}
	
	@RequestMapping(value = RequestMapper.GET_SMS_LIST, method = RequestMethod.GET)
	public @ResponseBody List<SMSDto> getAllSmsesByUserId(Principal principal) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return smsService.getSmsInfo(userDetails.getId());
	}
	
	//mobile
	@RequestMapping(value = RequestMapper.SAVE_SMS, method = RequestMethod.POST)
	@ResponseBody
	public List<SMSDto> saveSMSHistory(@PathVariable String userId, @RequestBody List<SMSDto> smsDtos) {
		return smsService.saveSMSHistory(userId, smsDtos);
	}
	
	@RequestMapping(value = RequestMapper.GET_SMS_HISTORY_FOR_MOBILE, method = RequestMethod.GET)
	public @ResponseBody List<SMSDto> getSmsByUserId(@RequestParam String currentPage,@PathVariable String userId){
		return smsService.getSmsInfo(userId,currentPage);
	}
}
