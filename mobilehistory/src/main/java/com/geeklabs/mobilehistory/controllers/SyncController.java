package com.geeklabs.mobilehistory.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.dto.SyncDto;
import com.geeklabs.mobilehistory.service.SyncService;
import com.geeklabs.mobilehistory.util.RequestMapper;


@Controller
@RequestMapping(value = RequestMapper.SYNC)
public class SyncController {

	@Autowired
	private SyncService syncService;
	
	@RequestMapping(value = RequestMapper.SYNC_NOW, method = RequestMethod.POST)
	public @ResponseBody SyncDto syncAllData(@RequestBody SyncDto syncDto, @PathVariable String userId) {
		return syncService.syncNow(syncDto, userId);
	}
}
