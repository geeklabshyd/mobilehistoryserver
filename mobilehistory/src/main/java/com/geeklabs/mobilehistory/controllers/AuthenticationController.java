package com.geeklabs.mobilehistory.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.mobilehistory.dto.UserDto;
import com.geeklabs.mobilehistory.service.AuthenticationService;
import com.geeklabs.mobilehistory.util.RequestMapper;
import com.geeklabs.mobilehistory.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.AUTHENTICATION)
public class AuthenticationController {
	@Autowired
	private AuthenticationService authenticationservice;

	@ResponseBody
	@RequestMapping(value = RequestMapper.SIGNIN , method = RequestMethod.POST)
	public UserDto isAuthenticated(@RequestBody UserDto userDto) {
		UserDto dto = authenticationservice.isAuthenticated(userDto);
		return dto;
	}
	
	@RequestMapping(value = RequestMapper.SIGNOUT, method = RequestMethod.GET)
	public @ResponseBody ResponseStatus signOutByUserId(@PathVariable String userId) {
		ResponseStatus responseStatus = authenticationservice.signOutByUserId(userId);
		return responseStatus;
	}

}
