package com.geeklabs.mobilehistory.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class DeletedRecords {

	@Id
	private Long id;
	
	@Index
	private Long serverId;
	
	@Index
	private Long userId;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getServerId() {
		return serverId;
	}
	
	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}
}
