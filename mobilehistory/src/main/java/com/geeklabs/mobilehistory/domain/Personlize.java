package com.geeklabs.mobilehistory.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Personlize {

	private boolean allPersonalizes;
	private boolean recievedCalls;
	private boolean dialedCalls;
	private boolean missedCalls;
	private boolean sms;
	
	@Id
	private Long id;
	
	@Index
	private Long userId;
	private boolean contacts;
	private boolean isNwData;
	private boolean isWifi;
	
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isAllPersonalizes() {
		return allPersonalizes;
	}

	public void setAllPersonalizes(boolean allPersonalizes) {
		this.allPersonalizes = allPersonalizes;
	}

	public boolean isRecievedCalls() {
		return recievedCalls;
	}

	public void setRecievedCalls(boolean recievedCalls) {
		this.recievedCalls = recievedCalls;
	}

	public boolean isDialedCalls() {
		return dialedCalls;
	}

	public void setDialedCalls(boolean dialedCalls) {
		this.dialedCalls = dialedCalls;
	}

	public boolean isMissedCalls() {
		return missedCalls;
	}

	public void setMissedCalls(boolean missedCalls) {
		this.missedCalls = missedCalls;
	}

	public boolean isSms() {
		return sms;
	}

	public void setSms(boolean sms) {
		this.sms = sms;
	}

	public boolean isContacts() {
		return contacts;
	}
	
	public void setContacts(boolean contacts) {
		this.contacts = contacts;
	}
	
	public boolean isWifi() {
		return isWifi;
	}

	public void setWifi(boolean isWifi) {
		this.isWifi = isWifi;
	}

	public boolean isNwData() {
		return isNwData;
	}

	public void setNwData(boolean isNwData) {
		this.isNwData = isNwData;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personlize other = (Personlize) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
