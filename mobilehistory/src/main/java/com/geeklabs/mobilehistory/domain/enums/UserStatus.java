package com.geeklabs.mobilehistory.domain.enums;

public enum UserStatus {
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}
