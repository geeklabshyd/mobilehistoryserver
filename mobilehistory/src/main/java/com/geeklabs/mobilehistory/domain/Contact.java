package com.geeklabs.mobilehistory.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Contact {

	@Id
	private Long serverContactId;
	
	@Index
	private String name;
	@Index
	private String number;
	
	@Index
	private Long userId;
	private boolean isSyncked;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Long getServerContactId() {
		return serverContactId;
	}
	public void setServerContactId(Long serverContactId) {
		this.serverContactId = serverContactId;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public boolean isSyncked() {
		return isSyncked;
	}
	public void setSyncked(boolean isSyncked) {
		this.isSyncked = isSyncked;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((serverContactId == null) ? 0 : serverContactId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (serverContactId == null) {
			if (other.serverContactId != null)
				return false;
		} else if (!serverContactId.equals(other.serverContactId))
			return false;
		return true;
	}
	
	
}
