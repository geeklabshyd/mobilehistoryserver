package com.geeklabs.mobilehistory.repository;

import com.geeklabs.mobilehistory.domain.UserRole;
import com.geeklabs.mobilehistory.domain.enums.UserRoles;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;

public interface UserRoleRepository extends ObjectifyCRUDRepository<UserRole> {
	
	UserRole getUserRoleByRoleName(UserRoles roleName);
}
