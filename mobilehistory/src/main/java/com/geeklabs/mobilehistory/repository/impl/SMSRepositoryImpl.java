package com.geeklabs.mobilehistory.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.mobilehistory.domain.SMS;
import com.geeklabs.mobilehistory.repository.SMSRepository;
import com.geeklabs.mobilehistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;
import com.googlecode.objectify.Objectify;

@Repository
public class SMSRepositoryImpl extends AbstractObjectifyCRUDRepository<SMS> implements SMSRepository {

	@Autowired
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public SMSRepositoryImpl(){
		super(SMS.class);
	}

	
	@Override
	public List<SMS> getSmsInfo(Long userId, Pageable pageable) {
		List<SMS> sms = objectify.load().type(SMS.class)
				 .filter("userId", userId)
				 .order("-date")
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list(); // with given email user might not exist, use 'now', if not exist it returns null.
		return sms;
	}
	
	@Override
	public List<SMS> getSmsInfo(Long id) {
		List<SMS> sms = objectify.load()
								 .type(SMS.class)
								 .filter("userId", id)
								 .order("-date")
								 .list(); // with given email user might not exist, use 'now', if not exist it returns null.
		return sms;
	}

	@Override
	public SMS getSmsHistoryByDate(String date, Long userId) {
		return objectify.load()
				.type(SMS.class)
				.filter("date", date)
				.filter("userId", userId)
				.first()
				.now();
	}
	
	// FIXME - order by 
	@Override
	public List<SMS> getSmsInfoByContactName(Long userId, Pageable pageable, String searchTerm) {
		return  objectify.load()
						 .type(SMS.class)
						 .filter("userId", userId)
						 .filter("contactName >=", searchTerm)
						 .filter("contactName <", searchTerm + "\uFFFD")
						 .limit(Pageable.LIMIT)
						 .offset(pageable.getOffset())
						 .list(); // with given email user might not exist, use 'now', if not exist it returns null.

	}
	
	// FIXME - order by 
	@Override
	public List<SMS> getSmsInfoByMessage(Long userId, Pageable pageable, String searchTerm) {
		return  objectify.load()
				 .type(SMS.class)
				 .filter("userId", userId)
				 .filter("message >=", searchTerm)
				 .filter("message <", searchTerm + "\uFFFD")
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list();
	}

	// FIXME - order by 
	@Override
	public List<SMS> getSmsInfoByNumber(Long userId, Pageable pageable, String searchTerm) {
		return  objectify.load()
				 .type(SMS.class)
				 .filter("userId", userId)
				 .filter("from >=", searchTerm)
				 .filter("from <", searchTerm + "\uFFFD")
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list();
	}
}
