package com.geeklabs.mobilehistory.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.mobilehistory.domain.DeletedRecords;
import com.geeklabs.mobilehistory.repository.DeletedRecordsRepository;
import com.geeklabs.mobilehistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;
import com.googlecode.objectify.Objectify;

@Repository
public class DeletedRecordsRepositoryImpl extends AbstractObjectifyCRUDRepository<DeletedRecords> implements DeletedRecordsRepository {

	@Autowired
	private Objectify objectify;
	
	public DeletedRecordsRepositoryImpl(){
		super(DeletedRecords.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	@Override
	public List<DeletedRecords> getAllDeletedRecords(Long userId) {
		return 	 objectify.load()
				.type(DeletedRecords.class)
				.filter("userId", userId)
				.list();
	}
	
	@Override
	public List<DeletedRecords> getDeletedRecordsByPagable(long userId, Pageable pageable) {
		return 	 objectify.load()
				.type(DeletedRecords.class)
				.filter("userId", userId)
				.limit(Pageable.LIMIT)
				.offset(pageable.getOffset())
				.list();
	}
}