package com.geeklabs.mobilehistory.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.mobilehistory.domain.Contact;
import com.geeklabs.mobilehistory.repository.PhoneBookRepository;
import com.geeklabs.mobilehistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;
import com.googlecode.objectify.Objectify;

@Repository
public class PhoneBookRepositoryImpl extends AbstractObjectifyCRUDRepository<Contact> implements PhoneBookRepository {

	@Autowired
	private Objectify objectify;
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	public PhoneBookRepositoryImpl(){
		super(Contact.class);
	}
	
	@Override
	public List<Contact> findAllContactsByPagableAndSearchByName(Pageable pageable, Long userId, String searchTerm) {
			return objectify.load()
							.type(Contact.class)
							.filter("userId", userId)
							.limit(Pageable.LIMIT)
							.filter("name >=", searchTerm)
							.filter("name <", searchTerm + "\uFFFD")
							.offset(pageable.getOffset())
							.list(); // with given email user might not exist, use 'now', if not exist it returns null.
	}
	
	@Override
	public Contact isContactExist(String name, String number) {
					Contact contact = null; 	
					contact = objectify
									.load()
									.type(Contact.class)
									.filter("name", name)
									.first()
									.now();
					
					if (contact != null) {
						return contact;  
					}
					
					contact = objectify
							.load()
							.type(Contact.class)
							.filter("number", number)
							.first()
							.now();
					return contact;
		
	}

	@Override
	public List<Contact> findAllContactsByUserId(Long userId) {
		List<Contact> allContacts = objectify.load().type(Contact.class)
				 .filter("userId", userId)
				 .list(); // with given email user might not exist, use 'now', if not exist it returns null.
		return allContacts;
	}
	
	@Override
	public List<Contact> findAllContactsByPagable(Pageable pageable, Long userId) {
		return objectify.load()
				.type(Contact.class)
				.filter("userId", userId)
				.limit(Pageable.LIMIT)
				.offset(pageable.getOffset())
				.list(); // with given email user might not exist, use 'now', if not exist it returns null.
	}

	@Override
	public List<Contact> findAllContactsByPagableAndSearchByNumber(Pageable pageable, Long userId, String searchTerm) {
		return objectify.load()
				.type(Contact.class)
				.filter("userId", userId)
				.limit(Pageable.LIMIT)
				.filter("number >=", searchTerm)
				.filter("number <", searchTerm + "\uFFFD")
				.offset(pageable.getOffset())
				.list(); // with given email user might not exist, use 'now', if not exist it returns null.
	}
}
