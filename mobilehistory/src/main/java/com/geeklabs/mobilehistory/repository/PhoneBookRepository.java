package com.geeklabs.mobilehistory.repository;

import java.util.List;

import com.geeklabs.mobilehistory.domain.Contact;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;

public interface PhoneBookRepository extends ObjectifyCRUDRepository<Contact> {

	List<Contact> findAllContactsByPagableAndSearchByName(Pageable pageable, Long userId, String searchTerm);

	Contact isContactExist(String name, String number);
	
	List<Contact> findAllContactsByUserId(Long userId);

	List<Contact> findAllContactsByPagable(Pageable pageable, Long userId);

	List<Contact> findAllContactsByPagableAndSearchByNumber(Pageable pageable, Long userId, String searchTerm);

}
