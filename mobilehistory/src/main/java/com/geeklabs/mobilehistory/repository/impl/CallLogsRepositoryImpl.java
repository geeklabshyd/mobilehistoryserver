package com.geeklabs.mobilehistory.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.mobilehistory.domain.CallLogs;
import com.geeklabs.mobilehistory.repository.CallLogsRepository;
import com.geeklabs.mobilehistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;
import com.googlecode.objectify.Objectify;

@Repository
public class CallLogsRepositoryImpl extends AbstractObjectifyCRUDRepository<CallLogs> implements CallLogsRepository {

	@Autowired
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public CallLogsRepositoryImpl(){
		super(CallLogs.class);
	}

	@Override
	public List<CallLogs> getCallLogsInfo(Long userId, Pageable pageable) {
		List<CallLogs> callHistories = objectify.load().type(CallLogs.class)
				 .filter("userId", userId)
				 .order("-date")
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list(); 
		return callHistories;
	}

	@Override
	public List<CallLogs> getCallsByStatus(Long userId, int status, Pageable pageable) {
		
		List<CallLogs> callHistories = objectify.load().type(CallLogs.class)
				 .filter("userId", userId)
				 .filter("callType", status)
				 .order("-date")
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list(); 
		return callHistories;
	}
	
	@Override
	public List<CallLogs> getCallLogsInfo(Long userId) {
		List<CallLogs> callHistories = objectify.load().type(CallLogs.class)
				 .filter("userId", userId)
				 .order("-date")
				 .list(); 
		return callHistories;
	}

	@Override
	public CallLogs getCallLogsByDate(String date, Long userId) {
		return objectify.load()
						.type(CallLogs.class)
						.filter("date", date)
						.filter("userId", userId)
						.first()
						.now();
	}
	
	@Override
	public List<CallLogs> getCallLogsByPagable(Long userId, Pageable pageable) {
		return 	objectify.load()
				 .type(CallLogs.class)
				 .filter("userId", userId)
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list();
	}
	
	@Override
	public List<CallLogs> getCallLogsByName(Long userId, Pageable pageable, String searchTerm) {
		return  objectify.load()
				 .type(CallLogs.class)
				 .filter("userId", userId)
				 .filter("contactName >=", searchTerm)
				 .filter("contactName <", searchTerm + "\uFFFD") 
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list();
	}
	
	@Override
	public List<CallLogs> getCallLogsByNumber(Long userId, Pageable pageable, String searchTerm) {
		return  objectify.load()
				 .type(CallLogs.class)
				 .filter("userId", userId)
				 .filter("number >=", searchTerm)
				 .filter("number <", searchTerm + "\uFFFD") 
				 .limit(Pageable.LIMIT)
				 .offset(pageable.getOffset())
				 .list();
	}
}
