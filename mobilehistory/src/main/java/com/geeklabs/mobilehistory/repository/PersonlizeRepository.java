package com.geeklabs.mobilehistory.repository;

import com.geeklabs.mobilehistory.domain.Personlize;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;

public interface PersonlizeRepository extends ObjectifyCRUDRepository<Personlize> {
	Personlize getPerByUser(Long id);
}



