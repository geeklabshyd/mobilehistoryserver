package com.geeklabs.mobilehistory.repository;

import com.geeklabs.mobilehistory.domain.User;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User>{

	User getUserByEmailOrUserName(String userName, String email);
	User getUserByName(String userName);
	User getUserByEmail(String email);
}
