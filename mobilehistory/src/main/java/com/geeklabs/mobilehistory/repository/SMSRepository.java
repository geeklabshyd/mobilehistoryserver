package com.geeklabs.mobilehistory.repository;

import java.util.List;

import com.geeklabs.mobilehistory.domain.SMS;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;

public interface SMSRepository extends ObjectifyCRUDRepository<SMS> {

	List<SMS> getSmsInfo(Long id, Pageable pageable);

	List<SMS> getSmsInfo(Long id);

	SMS getSmsHistoryByDate(String date, Long userId);

	List<SMS> getSmsInfoByContactName(Long userId, Pageable pageable, String searchTerm);

	List<SMS> getSmsInfoByMessage(Long userId, Pageable pageable, String searchType);

	List<SMS> getSmsInfoByNumber(Long userId, Pageable pageable, String searchType);

}
