package com.geeklabs.mobilehistory.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.mobilehistory.domain.Personlize;
import com.geeklabs.mobilehistory.repository.PersonlizeRepository;
import com.geeklabs.mobilehistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class PersonlizeRepositoryImpl  extends AbstractObjectifyCRUDRepository<Personlize> implements PersonlizeRepository {

	
	@Autowired
	private Objectify objectify;
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public PersonlizeRepositoryImpl() {
		super(Personlize.class);
	}

	@Override
	public Personlize getPerByUser(Long id) {
		Personlize sms = objectify.load().type(Personlize.class)
				 .filter("userId", id).first().now();
		return sms;
	}
}
