package com.geeklabs.mobilehistory.repository;

import java.util.List;

import com.geeklabs.mobilehistory.domain.DeletedRecords;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;

public interface DeletedRecordsRepository extends ObjectifyCRUDRepository<DeletedRecords> {
	
	List<DeletedRecords> getAllDeletedRecords(Long userId);

	List<DeletedRecords> getDeletedRecordsByPagable(long parseLong, Pageable pageable);

}
