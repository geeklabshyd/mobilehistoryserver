package com.geeklabs.mobilehistory.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.mobilehistory.domain.User;
import com.geeklabs.mobilehistory.domain.UserRole;
import com.geeklabs.mobilehistory.domain.enums.UserRoles;
import com.geeklabs.mobilehistory.repository.UserRoleRepository;
import com.geeklabs.mobilehistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class UserRoleRepositoryImpl extends AbstractObjectifyCRUDRepository<UserRole> implements UserRoleRepository {
	@Autowired
	private Objectify objectify;
	
	public UserRoleRepositoryImpl() {
		super(UserRole.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public User getAdminByEmail(String email) {
		return objectify.load()
				.type(User.class)
				.filter("email", email)
				.first()
				.now();
	}

	@Override
	public UserRole getUserRoleByRoleName(UserRoles roleName) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}