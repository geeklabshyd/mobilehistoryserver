package com.geeklabs.mobilehistory.repository;

import java.util.List;

import com.geeklabs.mobilehistory.domain.CallLogs;
import com.geeklabs.mobilehistory.repository.objectify.ObjectifyCRUDRepository;
import com.geeklabs.mobilehistory.util.Pageable;

public interface CallLogsRepository extends ObjectifyCRUDRepository<CallLogs>{

	List<CallLogs> getCallLogsInfo(Long userId, Pageable pageable);

	List<CallLogs> getCallsByStatus(Long userId, int status, Pageable pageable);

	List<CallLogs> getCallLogsInfo(Long userId);

	CallLogs getCallLogsByDate(String date, Long userId);

	List<CallLogs> getCallLogsByPagable(Long userId, Pageable pageable);

	List<CallLogs> getCallLogsByNumber(Long userId, Pageable pageable, String searchTerm);

	List<CallLogs> getCallLogsByName(Long userId, Pageable pageable, String searchTerm);
}
