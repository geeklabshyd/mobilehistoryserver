package com.geeklabs.mobilehistory.service;

import com.geeklabs.mobilehistory.domain.UserRole;
import com.geeklabs.mobilehistory.domain.enums.UserRoles;

public interface UserRoleService {

	UserRole getUserRoleByRoleName(UserRoles roles);

	
}
