package com.geeklabs.mobilehistory.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.DeletedRecords;
import com.geeklabs.mobilehistory.dto.DeletedRecordsDto;
import com.geeklabs.mobilehistory.dto.SyncDto;
import com.geeklabs.mobilehistory.service.CallLogsService;
import com.geeklabs.mobilehistory.service.DeletedRecordsService;
import com.geeklabs.mobilehistory.service.SmsService;
import com.geeklabs.mobilehistory.service.SyncService;

@Service
public class SyncServiceImpl implements SyncService {

	@Autowired
	private SmsService smsService;

	@Autowired
	private CallLogsService callLogsService;

	@Autowired
	private DeletedRecordsService deletedRecordsService;

	@Override
	@Transactional
	public SyncDto syncNow(SyncDto syncDto, String userId) {

		if (syncDto.isSmsSynced() || syncDto.isAllSynced()) {
			// Sync SMS -> Save sms, get deleted sms
			smsService.saveAll(userId, syncDto.getSmses());
		}

		if (syncDto.isCallsSynced() || syncDto.isAllSynced()) {
			// Sync Calls
			callLogsService.saveAll(userId, syncDto.getCallLogs());
		}
			// Set to Sync Dto
			// Get all Deleted records at server side
			List<DeletedRecordsDto> allDeletedRecords = deletedRecordsService.getAllDeletedRecordsForMoble(Long.parseLong(userId));
			
		return syncDto;
	}
}
