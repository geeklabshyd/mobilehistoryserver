package com.geeklabs.mobilehistory.service;

import com.geeklabs.mobilehistory.dto.UserDto;


public interface EmailService {
	
	void sendWelcomeMail(UserDto userDto);
	
	void send(String toEmail, String subject, String body);

	void sendForgotPasswordMail(UserDto userDto);
}
