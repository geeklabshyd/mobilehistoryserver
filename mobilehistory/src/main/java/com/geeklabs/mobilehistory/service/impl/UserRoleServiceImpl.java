package com.geeklabs.mobilehistory.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.UserRole;
import com.geeklabs.mobilehistory.domain.enums.UserRoles;
import com.geeklabs.mobilehistory.repository.UserRoleRepository;
import com.geeklabs.mobilehistory.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Override
	@Transactional(readOnly = true)
	public UserRole getUserRoleByRoleName(UserRoles roleName) {
		return userRoleRepository.getUserRoleByRoleName(roleName);
	}
}
