package com.geeklabs.mobilehistory.service;

import com.geeklabs.mobilehistory.dto.PersonalizeDto;
import com.geeklabs.mobilehistory.util.ResponseStatus;

public interface PersonlizeService {

	ResponseStatus savepersonlize(String userId, PersonalizeDto personlizeDto);

	PersonalizeDto getPerByUser(Long id);
}
