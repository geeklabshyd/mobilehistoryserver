package com.geeklabs.mobilehistory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.DeletedRecords;
import com.geeklabs.mobilehistory.dto.DeletedRecordsDto;
import com.geeklabs.mobilehistory.repository.DeletedRecordsRepository;
import com.geeklabs.mobilehistory.service.DeletedRecordsService;
import com.geeklabs.mobilehistory.util.Pageable;

@Service
public class DeletedRecordsServiceImpl implements DeletedRecordsService {

	@Autowired
	private DeletedRecordsRepository deletedRecordsRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@Transactional
	public void addDeletedRecord(Long serverId, Long userId) {
		DeletedRecords deletedRecords = new DeletedRecords();
		deletedRecords.setServerId(serverId);
		deletedRecords.setUserId(userId);
		deletedRecordsRepository.save(deletedRecords);
	}

	@Override
	@Transactional
	public void deleteAllByUserId(Long userId) {
		if (userId != null && userId > 0) {
			List<DeletedRecords> allDeletedCalls = getAllDeletedRecords(userId);
			deletedRecordsRepository.delete(allDeletedCalls);
		}
	}

	private List<DeletedRecords> getAllDeletedRecords(Long userId) {
		List<DeletedRecords> allDeletedRecords = deletedRecordsRepository.getAllDeletedRecords(userId);
		return allDeletedRecords;
	}

	@Override
	@Transactional(readOnly = true)
	public List<DeletedRecordsDto> getAllDeletedRecordsForMoble(Long userId) {
		List<DeletedRecords> allDeletedRecords = deletedRecordsRepository.getAllDeletedRecords(userId);

		List<DeletedRecordsDto> deletedRecordDtos = new ArrayList<DeletedRecordsDto>();
		for (DeletedRecords deletedRecords : allDeletedRecords) {
			DeletedRecordsDto deletedRecordsDto = dozerBeanMapper.map(deletedRecords, DeletedRecordsDto.class);
			deletedRecordDtos.add(deletedRecordsDto);
		}
		return deletedRecordDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public List<DeletedRecordsDto> getDeletedRecordsByPagable(String userId, int currentPage) {
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		
		List<DeletedRecords> allDeletedRecords = deletedRecordsRepository.getDeletedRecordsByPagable(Long.parseLong(userId), pageable);

		List<DeletedRecordsDto> deletedRecordDtos = new ArrayList<DeletedRecordsDto>();
		for (DeletedRecords deletedRecords : allDeletedRecords) {
			DeletedRecordsDto deletedRecordsDto = dozerBeanMapper.map(deletedRecords, DeletedRecordsDto.class);
			deletedRecordDtos.add(deletedRecordsDto);
		}
		return deletedRecordDtos;
	}
}
