package com.geeklabs.mobilehistory.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.geeklabs.mobilehistory.domain.CustomUserDetails;
import com.geeklabs.mobilehistory.domain.User;
import com.geeklabs.mobilehistory.dto.ChangePwdDto;
import com.geeklabs.mobilehistory.dto.ForgotPasswordDto;
import com.geeklabs.mobilehistory.util.ResponseStatus;

public interface UserService extends UserDetailsService {

	void setupAppUserRoles();
	void setupAppUser();
	
	User getUserByEamailOrUserName(String userName, String email);
	User getUserByName(String userName);
	ResponseStatus changePwd(ChangePwdDto changePwdDto, CustomUserDetails userDetails);
	ResponseStatus sendforgotPasswordToUserMail(ForgotPasswordDto forgotPasswordDto);
	
}
