package com.geeklabs.mobilehistory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.CallLogs;
import com.geeklabs.mobilehistory.dto.CallLogsDto;
import com.geeklabs.mobilehistory.repository.CallLogsRepository;
import com.geeklabs.mobilehistory.service.CallLogsService;
import com.geeklabs.mobilehistory.service.DeletedRecordsService;
import com.geeklabs.mobilehistory.transformers.CallTransformer;
import com.geeklabs.mobilehistory.util.Pageable;
import com.geeklabs.mobilehistory.util.ResponseStatus;

@Service
public class CallLogsServiceImpl implements CallLogsService {

	@Autowired
	private CallLogsRepository callLogsRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private DeletedRecordsService deletedRecordsService;

	@Override
	@Transactional
	public List<CallLogsDto> saveCallLogs(String id, List<CallLogsDto> CallLogsDtos) {
		saveAll(id, CallLogsDtos);
		return CallLogsDtos;

	}

	@Override
	@Transactional
	public void saveAll(String id, List<CallLogsDto> CallLogsDtos) {
		for (CallLogsDto callLogDto : CallLogsDtos) {
			// check is CallLogs exist
			CallLogs callLogs = isCallLogsExist(callLogDto.getDate(), Long.valueOf(id));
			String number = callLogDto.getNumber();
			Long contactNum = Long.valueOf(number);
			if (callLogs == null && contactNum != null && contactNum > 0) {
				// save call history place syncked true
				callLogs = dozerBeanMapper.map(callLogDto, CallLogs.class);
				callLogs.setSyncked(true);
				callLogsRepository.save(callLogs);

				// set servercall id to CallDto
				callLogDto.setSyncked(true);
				callLogDto.setServerCallId(callLogs.getServerCallId());
			}
		}
	}

	private CallLogs isCallLogsExist(String date, Long userId) {
		return callLogsRepository.getCallLogsByDate(date, userId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CallLogsDto> getCallLogs(Long userId, int pageNo) {

		Pageable pageable = new Pageable();
		pageable.setOffset(pageNo - 1);

		List<CallLogs> CallLogsInfo = callLogsRepository.getCallLogsInfo(userId, pageable);
		List<CallLogsDto> CallLogsDtoList = CallTransformer.convertCallListIntoCallLogsDtoList(dozerBeanMapper, CallLogsInfo);

		return CallLogsDtoList;
	}

	@Override
	@Transactional
	public ResponseStatus deleteCall(Long callId, Long userId) {

		// before delete insert data into deletedrecord table
		try {
			deletedRecordsService.addDeletedRecord(callId, userId);
		} catch (Exception e) {
		}
		ResponseStatus responseStatus = new ResponseStatus();
		callLogsRepository.delete(callId);
		responseStatus.setStatus("success");

		return responseStatus;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CallLogsDto> getCallsByStatus(Long userId, String searchType, int pageNo, String searchTerm) {

		Pageable pageable = new Pageable();
		pageable.setOffset(pageNo - 1);

		List<CallLogsDto> CallLogsDtos = new ArrayList<>();
		List<CallLogs> callHistories = new ArrayList<>();

		if (searchType != null && !searchType.isEmpty() && searchType.equals("Number")) {
				callHistories = callLogsRepository.getCallLogsByNumber(userId, pageable, searchTerm);
		} else if (searchType != null && !searchType.isEmpty() && searchType.equals("Name")) {
			callHistories = callLogsRepository.getCallLogsByName(userId, pageable, searchTerm);
		} else {
			callHistories = callLogsRepository.getCallLogsInfo(userId, pageable);
		}
		CallLogsDtos = CallTransformer.convertCallListIntoCallLogsDtoList(dozerBeanMapper, callHistories);
		return CallLogsDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public CallLogs getCallLogsById(Long callId) {
		if (callId != null) {
			return callLogsRepository.findOne(callId);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CallLogsDto> getCallLogsInfo(Long userId) {
		List<CallLogs> CallLogsInfo = callLogsRepository.getCallLogsInfo(userId);
		List<CallLogsDto> CallLogsDtos = CallTransformer.convertCallListIntoCallLogsDtoList(dozerBeanMapper, CallLogsInfo);
		return CallLogsDtos;
	}

	// for mobile Req
	@Override
	@Transactional(readOnly = true)
	public List<CallLogsDto> getCallLogsByPagable(String userId, int currentPage) {
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		List<CallLogs> callLogsByPagable = callLogsRepository.getCallLogsByPagable(Long.valueOf(userId), pageable);
		List<CallLogsDto> CallLogsDtos = CallTransformer.convertCallListIntoCallLogsDtoList(dozerBeanMapper, callLogsByPagable);
		return CallLogsDtos;
	}
}
