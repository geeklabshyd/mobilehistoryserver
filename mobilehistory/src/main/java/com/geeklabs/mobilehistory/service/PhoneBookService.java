package com.geeklabs.mobilehistory.service;

import java.util.List;

import com.geeklabs.mobilehistory.dto.ContactDto;

public interface PhoneBookService {

	List<ContactDto> savePhoneBook(String userId, List<ContactDto> contactDtos);
	List<ContactDto> getAllContactsByPagableAndSearchType(Long userId, Integer currentPage, String searchType, String searchTerm);
	List<ContactDto> getAllContactsByUserId(Long userId);
	List<ContactDto> getContactsByPagable(String userId, String currentPage);
}

