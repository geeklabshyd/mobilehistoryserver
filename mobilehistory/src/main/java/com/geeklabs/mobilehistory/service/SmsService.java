package com.geeklabs.mobilehistory.service;

import java.util.List;

import com.geeklabs.mobilehistory.domain.SMS;
import com.geeklabs.mobilehistory.dto.SMSDto;
import com.geeklabs.mobilehistory.util.ResponseStatus;

public interface SmsService {
	
	List<SMSDto> getSmsInfo(Long id);
	
	List<SMSDto> saveSMSHistory(String id, List<SMSDto> smsDtos);
	void saveAll(String id, List<SMSDto> smsDtos);
	
	List<SMSDto> getAllSMS(Long userId, int pageNo, String searchType, String searchTerm);
	
	ResponseStatus deleteSms(Long smsId, Long userId);

	List<SMSDto> getSmsInfo(String userId, String currentPage);

	SMS getSmsById(Long smsId);

}
