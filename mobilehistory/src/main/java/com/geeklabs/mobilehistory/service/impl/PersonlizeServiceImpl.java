package com.geeklabs.mobilehistory.service.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.Personlize;
import com.geeklabs.mobilehistory.dto.PersonalizeDto;
import com.geeklabs.mobilehistory.repository.PersonlizeRepository;
import com.geeklabs.mobilehistory.service.PersonlizeService;
import com.geeklabs.mobilehistory.util.ResponseStatus;

@Service
public class PersonlizeServiceImpl implements PersonlizeService {
	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private PersonlizeRepository personlizeRepository;

	@Override
	@Transactional
	public ResponseStatus savepersonlize(String userId, PersonalizeDto personlizeDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		Personlize personlize = null;
		Long perUserId = personlizeDto.getUserId();
		personlize = personlizeRepository.getPerByUser(perUserId);
		if (personlize != null) {
			dozerBeanMapper.map(personlizeDto, personlize);
		} else {
			personlize = dozerBeanMapper.map(personlizeDto, Personlize.class);
			personlize.setUserId(Long.valueOf(userId));
		}

		personlizeRepository.save(personlize);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	public PersonalizeDto getPerByUser(Long id) {
		Personlize personlize = personlizeRepository.getPerByUser(id);
		PersonalizeDto personalizeDto = new PersonalizeDto();
		if (personlize != null) {
			personalizeDto = dozerBeanMapper.map(personlize, PersonalizeDto.class);
		}
		return personalizeDto;

	}
}
