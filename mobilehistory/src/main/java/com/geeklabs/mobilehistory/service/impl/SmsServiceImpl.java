package com.geeklabs.mobilehistory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.SMS;
import com.geeklabs.mobilehistory.dto.SMSDto;
import com.geeklabs.mobilehistory.repository.SMSRepository;
import com.geeklabs.mobilehistory.service.DeletedRecordsService;
import com.geeklabs.mobilehistory.service.SmsService;
import com.geeklabs.mobilehistory.transformers.SMSTransformer;
import com.geeklabs.mobilehistory.util.Pageable;
import com.geeklabs.mobilehistory.util.ResponseStatus;

@Service
public class SmsServiceImpl implements SmsService {

	@Autowired
	private SMSRepository smsRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private DeletedRecordsService deletedRecordsService;

	@Override
	@Transactional(readOnly = true)
	public List<SMSDto> getSmsInfo(Long id) {
		List<SMS> smsList = smsRepository.getSmsInfo(id);
		List<SMSDto> smsDtos = SMSTransformer.convertSMSListIntoSMSDtosList(smsList, dozerBeanMapper);
		return smsDtos;
	}

	@Override
	@Transactional
	public List<SMSDto> saveSMSHistory(String userId, List<SMSDto> smsDtos) {
		saveAll(userId, smsDtos);
		return smsDtos;

	}

	@Override
	@Transactional
	public void saveAll(String userId, List<SMSDto> smsDtos) {
		for (SMSDto smsDto : smsDtos) {
			// check is SMS exist
			SMS sms = isSmsExist(smsDto.getDate(), Long.valueOf(userId));
			if (sms == null) {
				// save call history place syncked true
				sms = new SMS();
				dozerBeanMapper.map(smsDto, sms);
				sms.setSyncked(true);
				sms.setServerSmsId(null);
				smsRepository.save(sms);

				// set servercall id to SMSDto
				smsDto.setSyncked(true);
				smsDto.setServerSmsId(sms.getServerSmsId());
			}
		}
	}

	private SMS isSmsExist(String date, Long userId) {
		return smsRepository.getSmsHistoryByDate(date, userId);
	}

	@Override
	@Transactional
	public List<SMSDto> getAllSMS(Long userId, int pageNo, String searchType, String searchTerm) {
		List<SMS> smsInfo = new ArrayList<>();
		Pageable pageable = new Pageable();
		pageable.setOffset(pageNo - 1);
		
		if (searchType != null && !searchType.isEmpty() && searchType.equals("Message")) {
			smsInfo = smsRepository.getSmsInfoByMessage(userId, pageable, searchTerm);
		} else if (searchType != null && !searchType.isEmpty() && searchType.equals("Number")) {
				smsInfo = smsRepository.getSmsInfoByNumber(userId, pageable, searchTerm);
		} else if (searchType != null && !searchType.isEmpty() && searchType.equals("Name")) {
			smsInfo = smsRepository.getSmsInfoByContactName(userId, pageable, searchTerm);
		} else {
			smsInfo = smsRepository.getSmsInfo(userId, pageable);
		}
		List<SMSDto> smsDtosList = SMSTransformer.convertSMSListIntoSMSDtosList(smsInfo, dozerBeanMapper);
	return smsDtosList;
	}

	@Override
	@Transactional
	public ResponseStatus deleteSms(Long serverId, Long userId) {
		// before delete insert data into deletedrecord table
		try {
			deletedRecordsService.addDeletedRecord(serverId, userId);
		} catch (Exception exception) {

		}
		ResponseStatus responseStatus = new ResponseStatus();
		smsRepository.delete(serverId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SMSDto> getSmsInfo(String userId, String currentPage) {
		Pageable pageable = new Pageable();
		pageable.setOffset(Integer.valueOf(currentPage));
		List<SMS> smsHistory = smsRepository.getSmsInfo(Long.valueOf(userId), pageable);
		List<SMSDto> smsDtos = SMSTransformer.convertSMSListIntoSMSDtosList(smsHistory, dozerBeanMapper);
		return smsDtos;

	}

	@Override
	@Transactional(readOnly = true)
	public SMS getSmsById(Long smsId) {
		return smsRepository.findOne(smsId);
	}
}
