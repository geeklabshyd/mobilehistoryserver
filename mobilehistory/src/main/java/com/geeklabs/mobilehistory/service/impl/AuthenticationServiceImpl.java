package com.geeklabs.mobilehistory.service.impl;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.User;
import com.geeklabs.mobilehistory.domain.enums.UserRoles;
import com.geeklabs.mobilehistory.dto.PersonalizeDto;
import com.geeklabs.mobilehistory.dto.UserDto;
import com.geeklabs.mobilehistory.repository.UserRepository;
import com.geeklabs.mobilehistory.repository.UserRoleRepository;
import com.geeklabs.mobilehistory.service.AuthenticationService;
import com.geeklabs.mobilehistory.service.EmailService;
import com.geeklabs.mobilehistory.service.PersonlizeService;
import com.geeklabs.mobilehistory.transformer.UserConverter;
import com.geeklabs.mobilehistory.util.ResponseStatus;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JsonFactory FACTORY = new JacksonFactory();
	@Autowired
	private UserRepository userrepository;
	@Autowired
	private UserRoleRepository userrolerepository;
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private PersonlizeService personlizeService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;  
	
	@Override
	@Transactional
	public UserDto isAuthenticated(UserDto userDto) {
		Userinfoplus userinfo = getUserInfofromAccessToken(userDto.getAccesstoken());
		// check user exist or not
		User user = getUserByEmail(userinfo.getEmail());

		if (user != null) {
			user.setSignIn(true);
			user.setPicUrl(userinfo.getPicture());
			// convert user into userDto
			userDto = dozerBeanMapper.map(user, UserDto.class);
			userDto.setPicUrl(userinfo.getPicture());
			userDto.setStatus("success");
			
			PersonalizeDto personalizeDto = personlizeService.getPerByUser(user.getId());
			userDto.setPersonaizeDto(personalizeDto);
			
			userrepository.save(user);
			return userDto;
		} else {
			// create user
			user = new User();
			user.setEmail(userinfo.getEmail());
			user.setFirstName(userinfo.getName());
			user.setLastName(userinfo.getFamilyName());
			user.setSignIn(true);
			user.setUserName(userinfo.getGivenName());
			user.setPassword(RandomStringUtils.randomAlphanumeric(8));
			user.setUserRole(UserRoles.USER);
			user.setPicUrl(userinfo.getPicture());
			
			
			emailService.sendWelcomeMail(UserConverter.convertUserToUserDto(user));
			
//			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			userrepository.save(user);
			userDto = dozerBeanMapper.map(user, UserDto.class);
			
			// Save first time Personalized 
			PersonalizeDto personalizeDto = new PersonalizeDto();
			personalizeDto.setAllPersonalizes(true);
			personalizeDto.setWifi(true);
			personalizeDto.setUserId(user.getId());
			ResponseStatus responseStatus = personlizeService.savepersonlize(String.valueOf(user.getId()), personalizeDto);
			if (responseStatus.getStatus().equals("success")) {
				PersonalizeDto personalizeDto1 = personlizeService.getPerByUser(user.getId());
				userDto.setPersonaizeDto(personalizeDto1);
			}
			
			userDto.setPicUrl(userinfo.getPicture());
			userDto.setStatus("success");
			userDto.setUserRegistering(true);
			return userDto;
		}
	}

	private User getUserByEmail(String email) {
		User user = userrepository.getUserByEmail(email);
		return user;
	}

	private Userinfoplus getUserInfofromAccessToken(String accesstoken) {
		GoogleCredential googleCredential = new GoogleCredential()
				.setAccessToken(accesstoken);
		Oauth2 oauth2 = new Oauth2.Builder(HTTP_TRANSPORT, FACTORY, googleCredential).build();

		Userinfoplus userinfo = null;
		try {
			userinfo = oauth2.userinfo().get().setOauthToken(accesstoken).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userinfo;
	}
	
	@Override
	@Transactional
	public ResponseStatus signOutByUserId(String userId) {
		ResponseStatus responseStatus = new ResponseStatus();
		User user = userrepository.findOne(Long.valueOf(userId));
		user.setSignIn(false);
		userrepository.save(user);
		responseStatus.setStatus("success");
		return responseStatus;
	}
}
