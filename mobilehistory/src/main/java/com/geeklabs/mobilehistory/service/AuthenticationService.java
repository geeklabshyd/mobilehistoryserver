package com.geeklabs.mobilehistory.service;

import com.geeklabs.mobilehistory.dto.UserDto;
import com.geeklabs.mobilehistory.util.ResponseStatus;

public interface AuthenticationService {

	UserDto isAuthenticated(UserDto userDto);

	ResponseStatus signOutByUserId(String userId);

}
