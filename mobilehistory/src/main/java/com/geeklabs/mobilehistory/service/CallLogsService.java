package com.geeklabs.mobilehistory.service;

import java.util.List;

import com.geeklabs.mobilehistory.domain.CallLogs;
import com.geeklabs.mobilehistory.dto.CallLogsDto;
import com.geeklabs.mobilehistory.util.ResponseStatus;

public interface CallLogsService {

	List<CallLogsDto> saveCallLogs(String id, List<CallLogsDto> CallLogsDtos);
	void saveAll(String id, List<CallLogsDto> CallLogsDtos);
	List<CallLogsDto> getCallLogsInfo(Long id);

	List<CallLogsDto> getCallLogs(Long userId, int pageNo);

	ResponseStatus deleteCall(Long callId, Long userId);

	List<CallLogsDto> getCallsByStatus(Long id, String status, int pageNo, String searchTerm);

	CallLogs getCallLogsById(Long callId);
	List<CallLogsDto> getCallLogsByPagable(String userId, int currentPage);

}
