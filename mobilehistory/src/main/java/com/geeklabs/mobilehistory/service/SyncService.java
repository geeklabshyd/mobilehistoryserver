package com.geeklabs.mobilehistory.service;

import com.geeklabs.mobilehistory.dto.SyncDto;

public interface SyncService {

	SyncDto syncNow(SyncDto syncDto, String userId);

}
