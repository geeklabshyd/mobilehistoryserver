package com.geeklabs.mobilehistory.service;

import java.util.List;

import com.geeklabs.mobilehistory.dto.DeletedRecordsDto;

public interface DeletedRecordsService {

	void addDeletedRecord(Long serverId, Long userId);
	void deleteAllByUserId(Long userId);
	List<DeletedRecordsDto> getAllDeletedRecordsForMoble(Long userId);
	List<DeletedRecordsDto> getDeletedRecordsByPagable(String userId, int currentPage);

}
