package com.geeklabs.mobilehistory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.mobilehistory.domain.Contact;
import com.geeklabs.mobilehistory.dto.ContactDto;
import com.geeklabs.mobilehistory.repository.PhoneBookRepository;
import com.geeklabs.mobilehistory.service.PhoneBookService;
import com.geeklabs.mobilehistory.transformers.PhoneBookTransformer;
import com.geeklabs.mobilehistory.util.Pageable;

@Service
public class PhoneBookServiceImpl implements PhoneBookService {

	@Autowired
	private PhoneBookRepository phoneBookRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@Transactional
	public List<ContactDto> savePhoneBook(String userId, List<ContactDto> contactDtos) {

		for (ContactDto contactDto : contactDtos) {

			
			Contact contact = isContactExist(contactDto.getName(), contactDto.getNumber());
			
			if (contact != null) {
				if (!contact.getNumber().equals(contactDto.getNumber())) {
					contact.setNumber(contactDto.getNumber());
					updateContact(contact, userId);
					contactDto.setServerContactId(contact.getServerContactId());
					contactDto.setSyncked(true);
				}
				
				if (!contact.getName().equals(contactDto.getName())) {
					contact.setName(contactDto.getName());
					updateContact(contact, userId);
					contactDto.setServerContactId(contact.getServerContactId());
					contactDto.setSyncked(true);
				}
				
			}
			
			
			if (contact == null) {
				String number = contactDto.getNumber().trim();
				if (number != null && number.length() > 1) {
					contact = dozerBeanMapper.map(contactDto, Contact.class);
					contact.setSyncked(true);
					contact.setUserId(Long.valueOf(userId));
					phoneBookRepository.save(contact);
					contactDto.setServerContactId(contact.getServerContactId());
					contactDto.setSyncked(true);
				}
			}
		}
		
		List<ContactDto> allContactsByUserId = getAllContactsByUserId(Long.valueOf(userId));
		return allContactsByUserId;
	}

	private void updateContact(Contact contact, String userId) {
		contact = dozerBeanMapper.map(contact, Contact.class);
		contact.setSyncked(true);
		contact.setUserId(Long.valueOf(userId));
		phoneBookRepository.save(contact);
	}

	private Contact isContactExist(String name, String number) {
		return phoneBookRepository.isContactExist(name, number);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ContactDto> getAllContactsByPagableAndSearchType(Long userId, Integer currentPage, String searchType, String searchTerm) {
		List<Contact> contacts = new ArrayList<>();
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage - 1);

		if (searchType != null && !searchType.isEmpty() && searchType.equals("Number")) {
			contacts = phoneBookRepository.findAllContactsByPagableAndSearchByNumber(pageable, userId, searchTerm);
		} else if (searchType != null && !searchType.isEmpty() && searchType.equals("Name")) {
			contacts = phoneBookRepository.findAllContactsByPagableAndSearchByName(pageable, userId, searchTerm);
		} else {
			contacts = phoneBookRepository.findAllContactsByPagable(pageable, userId);
		}

		List<ContactDto> contactDtos = PhoneBookTransformer.convertContactsListIntoContactsDtoList(dozerBeanMapper, contacts);
		return contactDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public List<ContactDto> getAllContactsByUserId(Long userId) {
		List<Contact> allContactsByUserId = phoneBookRepository.findAllContactsByUserId(userId);
		List<ContactDto> contactDtos = PhoneBookTransformer.convertContactsListIntoContactsDtoList(dozerBeanMapper, allContactsByUserId);
		return contactDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public List<ContactDto> getContactsByPagable(String userId, String currentPage) {
		Pageable pageable = new Pageable();
		pageable.setOffset(Integer.parseInt(currentPage));
		
		List<Contact> contacts = phoneBookRepository.findAllContactsByPagable(pageable, Long.parseLong(userId));
		List<ContactDto> contactDtos = PhoneBookTransformer.convertContactsListIntoContactsDtoList(dozerBeanMapper, contacts);
		return contactDtos;
	}
}
